terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">0.73.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }

  }
}

provider "yandex" {
  zone = var.default-zone
}
module "vpc-stage" {
  #source                  = "git@gitlab.com:devops11100/infra/terraform_modules/yandex_cloud_vpc.git"
  source                  = "../../../../terraform_modules/yandex_cloud_vpc"
  vpc-network-name        = var.vpc-network-name
  vpc-subnet-name         = var.vpc-subnet-name
  vpc-network-description = var.vpc-network-description
  vpc-subnet-cidr         = var.vpc-subnet-cidr
  yandex-zones            = var.yandex-zones
  vpc-rout-table-name     = var.vpc-rout-table-name
  vpc-address-name        = var.vpc-address-name
}


module "vps-stage-vpn" {
  #source              = "git@gitlab.com:devops11100/infra/terraform_modules/yandex_cloud_vps.git"
  source              = "../../../../terraform_modules/yandex_cloud_vps"
  vps-name            = var.vps-vpn-name
  vps-platform-id     = var.vps-vpn-platform-id
  vps-zone            = var.vps-vpn-zone
  vps-resource-core   = var.vps-vpn-resource-core
  vps-resource-memory = var.vps-vpn-resource-memory
  vps-image-id        = var.vps-vpn-image-id
  vps-subnet-id       = module.vpc-stage.subnets[var.vps-vpn-zone][0]
  vps-public-ip-bool  = var.vps-vpn-public-ip-bool
  vps-public-ip       = module.vpc-stage.addr[var.vpc-address-name[0]]
  vps-labels          = var.vps-vpn-labels
  depends_on = [
    module.vpc-stage
  ]
}

module "vps-stage-redmine" {
  #source              = "git@gitlab.com:devops11100/infra/terraform_modules/yandex_cloud_vps.git"
  source              = "../../../../terraform_modules/yandex_cloud_vps"
  vps-name            = var.vps-redmine-name
  vps-platform-id     = var.vps-redmine-platform-id
  vps-zone            = var.vps-redmine-zone
  vps-resource-core   = var.vps-redmine-resource-core
  vps-resource-memory = var.vps-redmine-resource-memory
  vps-image-id        = var.vps-redmine-image-id
  vps-subnet-id       = module.vpc-stage.subnets[var.vps-redmine-zone][0]
  vps-public-ip-bool  = var.vps-redmine-public-ip-bool
  vps-public-ip       = module.vpc-stage.addr[var.vpc-address-name[0]]
  vps-labels          = var.vps-redmine-labels
  depends_on = [
    module.vpc-stage
  ]
}