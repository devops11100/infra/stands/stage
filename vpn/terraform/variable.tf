variable "vps-name" {
  type = string
  description = "VPS name"
}

variable "vps-platform-id" {
  type = string
  description = "VPS Platform ID"
}

variable "vps-zone" {
  type = string
  description = "VPS Zone"
}

variable "vps-resource-core" {
  type = number
  description = "VPS Resource core"
}

variable "vps-resource-memory" {
  type = number
  description = "VPS Resource memory"
}

variable "vps-image-id" {
  type = string
  description = "VPS Image id"
}

variable "vps-subnet-id" {
  type = number
  description = "VPS Subnet id"
}